provider "aws" {
 access_key = "${var.access_key}"
 secret_key = "${var.secret_key}"
 region     = "${var.region}"
}

#create VPC
resource "aws_vpc" "opstree" {
  cidr_block = "${var.vpc}"
  tags = { 
    name = "opstree"
 }
}

#internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.opstree.id}"
  tags = {
    Name = "op_ig"
  }
}

#Public subnet 1
resource "aws_subnet" "Pub-1" {
  vpc_id     = "${aws_vpc.opstree.id}"
  cidr_block = "${var.Pub-1}"
  availability_zone = "${var.AZ-1}"
  tags =  {
    Name = "pub-1"
 }
}

#Public subnet 2
resource "aws_subnet" "Pub-2" {
  vpc_id     = "${aws_vpc.opstree.id}"
  cidr_block = "${var.Pub-2}"
  availability_zone = "${var.AZ-2}"
  tags = {
    Name = "pub-2"
  }
}


#Private subnet 1
resource "aws_subnet" "Pvt-1" {
  vpc_id     = "${aws_vpc.opstree.id}"
  cidr_block = "${var.Pvt-1}"
  availability_zone = "${var.AZ-3}"
  tags = {
    Name = "pvt-1"
  }
}


#Private subnet 2
resource "aws_subnet" "Pvt-2" {
 vpc_id     = "${aws_vpc.opstree.id}"
  cidr_block = "${var.Pvt-2}"
  availability_zone = "${var.AZ-4}"
  tags = {
    Name = "pvt-2"
  }
}

# Public Route table
resource "aws_route_table" "Pub-RT" {
  vpc_id = "${aws_vpc.opstree.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }
  tags = {
    Name = "Opstree_ig"
  }
}


#Bastion-1 associate public-1
resource "aws_route_table_association" "Pub-a" {
  subnet_id      = "${aws_subnet.Pub-1.id}"
  route_table_id = "${aws_route_table.Pub-RT.id}"
}

#Bastion-2 associate public-2
resource "aws_route_table_association" "Pub-b" {
  subnet_id      = "${aws_subnet.Pub-2.id}"
  route_table_id = "${aws_route_table.Pub-RT.id}"
}

#create elastic ip
resource "aws_eip" "nat" {
  vpc      = true
}

#create nat gateway
resource "aws_nat_gateway" "nat_gw" {
  allocation_id = "${aws_eip.nat.id}"
  subnet_id     = "${aws_subnet.Pub-1.id}"
  tags = {
    Name = "gw NAT"
  }
}

# Private Route table associated with NAT
resource "aws_route_table" "Pvt-RT" {
  vpc_id = "${aws_vpc.opstree.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_nat_gateway.nat_gw.id}"
  }            
  tags =  {
    Name = "pnat-1"
  }
}
#Pvt Subnet associate with NAT
resource "aws_route_table_association" "npvt-1" {
  subnet_id      = "${aws_subnet.Pvt-1.id}"
  route_table_id = "${aws_route_table.Pvt-RT.id}"
}


#Pvt Subnet associate with NAT
resource "aws_route_table_association" "npvt-2" {
  subnet_id      = "${aws_subnet.Pvt-2.id}"
  route_table_id = "${aws_route_table.Pvt-RT.id}"
}


